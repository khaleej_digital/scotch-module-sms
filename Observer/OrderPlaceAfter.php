<?php
declare(strict_types=1);

namespace Beside\Sms\Observer;

use Beside\Sms\Api\MessageProviderInterface;
use Beside\Sms\Api\SmsInterface;
use Beside\Sms\Api\SmsSenderInterface;
use Beside\Sms\Model\SmsBuilder;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Psr\Log\LoggerInterface;
use Beside\Sms\Model\SmsFactory;

/**
 * Class AfterOrderPlace
 *
 * @package Beside\Sms\Observer
 */
class OrderPlaceAfter implements ObserverInterface
{
    /**
     * @var MessageProviderInterface
     */
    private MessageProviderInterface $messageProvider;

    /**
     * @var SmsBuilder
     */
    private SmsBuilder $smsDataBuilder;

    /**
     * @var SmsSenderInterface
     */
    private SmsSenderInterface $smsSender;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var SmsFactory
     */
    private SmsFactory $smsFactory;

    /**
     * OrderPlaceAfter constructor.
     *
     * @param MessageProviderInterface $messageProvider
     * @param SmsSenderInterface $smsSender
     * @param LoggerInterface $logger
     * @param SmsBuilder $smsDataBuilder
     * @param SmsFactory $smsFactory
     */
    public function __construct(
        MessageProviderInterface $messageProvider,
        SmsSenderInterface $smsSender,
        LoggerInterface $logger,
        SmsBuilder $smsDataBuilder,
        SmsFactory $smsFactory
    ) {
        $this->messageProvider = $messageProvider;
        $this->smsDataBuilder = $smsDataBuilder;
        $this->smsSender = $smsSender;
        $this->logger = $logger;
        $this->smsFactory = $smsFactory;
    }

    /**
     * Execute SMS sending after order placing
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer): void
    {
        /** @var OrderInterface $order */
        $order = $observer->getEvent()->getOrder();
        $data = $this->smsDataBuilder->getSmsDataFromOrder($order);
        $data[SmsInterface::MESSAGE] = $this->messageProvider->getMessageByType(
            MessageProviderInterface::SMS_TYPE_PLACE_ORDER,
            $data['customerData'],
            $data['storeId']
        );
        if ($this->smsDataBuilder->isSmsDataValid($data)) {
            /** @var SmsInterface $sms */
            $sms = $this->smsFactory->create();
            $sms->setNumber($data[SmsInterface::NUMBER]);
            $sms->setMessage($data[SmsInterface::MESSAGE]);
            $sms->setWebsiteCode($data[SmsInterface::WEBSITE_CODE]);
            $this->smsSender->sendSms($sms);
        } else {
            $this->logger->error(sprintf('Could not fetch data for SMS for order %s', $order->getIncrementId()));
        }
    }
}
