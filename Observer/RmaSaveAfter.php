<?php
declare(strict_types=1);

namespace Beside\Sms\Observer;

use Beside\Sms\Api\MessageProviderInterface;
use Beside\Sms\Api\SmsInterface;
use Beside\Sms\Api\SmsSenderInterface;
use Beside\Sms\Model\SmsBuilder;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Rma\Api\Data\RmaInterface;
use Psr\Log\LoggerInterface;
use Beside\Sms\Model\SmsFactory;

/**
 * Class RmaSaveAfter
 *
 * @package Beside\Sms\Observer
 */
class RmaSaveAfter implements ObserverInterface
{
    /**
     * @var MessageProviderInterface
     */
    private MessageProviderInterface $messageProvider;

    /**
     * @var SmsBuilder
     */
    private SmsBuilder $smsDataBuilder;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var SmsSenderInterface
     */
    private SmsSenderInterface $smsSender;

    /**
     * @var SmsFactory
     */
    private SmsFactory $smsFactory;

    /**
     * RmaSaveAfter constructor.
     *
     * @param MessageProviderInterface $messageProvider
     * @param LoggerInterface $logger
     * @param SmsSenderInterface $smsSender
     * @param SmsBuilder $smsDataBuilder
     * @param SmsFactory $smsFactory
     */
    public function __construct(
        MessageProviderInterface $messageProvider,
        LoggerInterface $logger,
        SmsSenderInterface $smsSender,
        SmsBuilder $smsDataBuilder,
        SmsFactory $smsFactory
    ) {
        $this->messageProvider = $messageProvider;
        $this->smsDataBuilder = $smsDataBuilder;
        $this->logger = $logger;
        $this->smsSender = $smsSender;
        $this->smsFactory = $smsFactory;
    }

    /**
     * Execute SMS sending
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer): void
    {
        /** @var RmaInterface $rma */
        $rma = $observer->getEvent()->getRma();
        if ($rma->isObjectNew()) {
            $order = $rma->getOrder();
            $data = $this->smsDataBuilder->getSmsDataFromOrder($order);
            $data[SmsInterface::MESSAGE] = $this->messageProvider->getMessageByType(
                MessageProviderInterface::SMS_TYPE_RMA_CREATE,
                $data['customerData'],
                $data['storeId']
            );
            if ($this->smsDataBuilder->isSmsDataValid($data)) {
                /** @var SmsInterface $sms */
                $sms = $this->smsFactory->create();
                $sms->setNumber($data[SmsInterface::NUMBER]);
                $sms->setMessage($data[SmsInterface::MESSAGE]);
                $sms->setWebsiteCode($data[SmsInterface::WEBSITE_CODE]);
                $this->smsSender->sendSms($sms);
            } else {
                $this->logger->error(sprintf('Could not fetch data for SMS for RMA order %s', $order->getIncrementId()));
            }
        }
    }
}
