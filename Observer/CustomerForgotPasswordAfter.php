<?php
declare(strict_types=1);

namespace Beside\Sms\Observer;

use Beside\Sms\Api\MessageProviderInterface;
use Beside\Customer\Setup\Patch\Data\AddTelephonePrefixAttribute;
use Beside\Sms\Api\SmsInterface;
use Beside\Sms\Api\SmsSenderInterface;
use Beside\Sms\Model\SmsBuilder;
use Exception;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Beside\Sms\Model\SmsFactory;

/**
 * Class CustomerSaveAfter
 *
 * @package Beside\Sms\Observer
 */
class CustomerForgotPasswordAfter implements ObserverInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private CustomerRepositoryInterface $customerRepository;

    /**
     * @var AddressRepositoryInterface
     */
    private AddressRepositoryInterface $addressRepository;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var MessageProviderInterface
     */
    private MessageProviderInterface $messageProvider;

    /**
     * @var SmsBuilder
     */
    private SmsBuilder $smsBuilder;

    /**
     * @var SmsSenderInterface
     */
    private SmsSenderInterface $smsSender;

    /**
     * @var SmsFactory
     */
    private SmsFactory $smsFactory;

    /**
     * CustomerSaveAfter constructor.
     *
     * @param CustomerRepositoryInterface $customerRepository
     * @param AddressRepositoryInterface $addressRepository
     * @param MessageProviderInterface $messageProvider
     * @param SmsBuilder $smsBuilder
     * @param StoreManagerInterface $storeManager
     * @param SmsSenderInterface $smsSender
     * @param SmsFactory $smsFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        AddressRepositoryInterface  $addressRepository,
        MessageProviderInterface $messageProvider,
        SmsBuilder $smsBuilder,
        StoreManagerInterface $storeManager,
        SmsSenderInterface $smsSender,
        SmsFactory $smsFactory,
        LoggerInterface $logger
    ) {
        $this->customerRepository = $customerRepository;
        $this->addressRepository = $addressRepository;
        $this->logger = $logger;
        $this->messageProvider = $messageProvider;
        $this->smsBuilder = $smsBuilder;
        $this->smsSender = $smsSender;
        $this->storeManager = $storeManager;
        $this->smsFactory = $smsFactory;
    }

    /**
     * Execute SMS sending
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer): void
    {
        $event = $observer->getEvent()->getRequest();
        $email = $event->getPost('email');
        if ($email) {
            try {
                $smsData['email'] = $email;
                $smsData[SmsInterface::NUMBER] = $this->getCustomerMobileNumberData($email);
                $smsData[SmsInterface::WEBSITE_CODE] = $this->getWebsiteCode();
                $storeId = $this->getStoreId();
                $smsData[SmsInterface::MESSAGE] = $this->messageProvider->getMessageByType(
                    MessageProviderInterface::SMS_TYPE_RESET_PASSWORD,
                    $smsData,
                    $storeId
                );
                if ($this->smsBuilder->isSmsDataValid($smsData)) {
                    /** @var SmsInterface $sms */
                    $sms = $this->smsFactory->create();
                    $sms->setNumber($smsData[SmsInterface::NUMBER]);
                    $sms->setMessage($smsData[SmsInterface::MESSAGE]);
                    $sms->setWebsiteCode($smsData[SmsInterface::WEBSITE_CODE]);
                    $this->smsSender->sendSms($sms);
                } else {
                    $this->logger->error(sprintf('Could not fetch data for SMS for customer %s', $email));
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
    }

    /**
     * Get customer mobile number
     *
     * @param string $email
     *
     * @return string|null
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomerMobileNumberData(string $email): ?string
    {
        $mobileNumber = null;
        $customer = $this->customerRepository->get($email);
        $shippingAddressId = $customer->getDefaultShipping();

        $address = $this->addressRepository->getById($shippingAddressId);
        $mobileNumber = $address->getTelephone();
        $prefixAttribute = $address->getCustomAttribute(AddTelephonePrefixAttribute::TELEPHONE_PREFIX_ATTRIBUTE);
        if ($prefixAttribute) {
            $mobilePrefix = $prefixAttribute->getValue();
            $mobileNumber = $mobilePrefix . $mobileNumber;
        }

        return $mobileNumber;
    }

    /**
     * Get current web site code
     *
     * @return string|null
     * @throws LocalizedException
     */
    private function getWebsiteCode(): ?string
    {
        $webSiteCode = null;
        $webSite = $this->storeManager->getWebsite();
        if ($webSite) {
            $webSiteCode = $webSite->getCode();
        }

        return $webSiteCode;
    }

    /**
     * Get current store code
     *
     * @return int|null
     * @throws LocalizedException
     */
    private function getStoreId(): ?int
    {
        $storeCode = null;
        $store = $this->storeManager->getStore();
        if ($store) {
            $storeCode = (int) $store->getId();
        }

        return $storeCode;
    }
}
