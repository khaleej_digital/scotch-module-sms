<?php
declare(strict_types=1);

namespace Beside\Sms\Model;

use Beside\Sms\Api\SmsInterface;
use Magento\Framework\Model\AbstractExtensibleModel;

/**
 * Class Sms
 *
 * @package Beside\Sms\Model
 */
class Sms extends AbstractExtensibleModel implements SmsInterface
{
    /**
     * Get message text
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->getData(self::MESSAGE);
    }

    /**
     * Set message text
     *
     * @param string $message
     *
     * @return SmsInterface
     */
    public function setMessage(string $message): SmsInterface
    {
        return $this->setData(self::MESSAGE, $message);
    }

    /**
     * Get phone number
     *
     * @return string
     */
    public function getNumber(): string
    {
        return (string) $this->getData(self::MESSAGE);
    }

    /**
     * Set phone number
     *
     * @param string $number
     *
     * @return SmsInterface
     */
    public function setNumber(string $number): SmsInterface
    {
        return $this->setData(self::NUMBER, $number);
    }

    /**
     * Get Website code
     *
     * @return string
     */
    public function getWebsiteCode(): string
    {
        return (string) $this->getData(self::WEBSITE_CODE);
    }

    /**
     * Set Website code
     *
     * @param string $websiteCode
     *
     * @return SmsInterface
     */
    public function setWebsiteCode(string $websiteCode): SmsInterface
    {
        return $this->setData(self::WEBSITE_CODE, $websiteCode);
    }
}
