<?php
declare(strict_types=1);

namespace Beside\Sms\Model;

use Beside\Sms\Api\SmsInterface;
use Beside\Sms\Api\SmsSenderInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Beside\Elitbuzztechnologies\Api\ElitbuzzConnectorInterface;
use Beside\Imissive\Api\ImissiveConnectorInterface;

/**
 * Class SmsSender
 *
 * @package Beside\Sms\Model
 */
class SmsSender implements SmsSenderInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var ElitbuzzConnectorInterface
     */
    private ElitbuzzConnectorInterface $elitbuzzConnector;

    /**
     * @var ImissiveConnectorInterface
     */
    private ImissiveConnectorInterface $imissiveConnector;

    /**
     * SmsSender constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param LoggerInterface $logger
     * @param ElitbuzzConnectorInterface $elitbuzzConnector
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        LoggerInterface $logger,
        ElitbuzzConnectorInterface $elitbuzzConnector,
        ImissiveConnectorInterface $imissiveConnector,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
        $this->elitbuzzConnector = $elitbuzzConnector;
        $this->imissiveConnector = $imissiveConnector;
    }

    /**
     * Send SMS to recipient
     *
     * @param SmsInterface $sms
     */
    public function sendSms(SmsInterface $sms): void
    {
        $websiteCode = $sms->getWebsiteCode();
        if ($this->isSmsEnabled($websiteCode)) {
            switch ($websiteCode) {
                case 'ae':
                    $result = $this->elitbuzzConnector->sendSms($sms->getNumber(), $sms->getMessage());
                    break;
                case 'sa':
                    $result = $this->imissiveConnector->sendSms($sms->getNumber(), $sms->getMessage());
                    break;
                default:
                    $this->logger->debug(__('%1 - is not a valid Website Code. Acaptebale values - \'ae\' or \'sa\'.', $websiteCode));
                    break;
            }
            if (!empty($result)) {
                $this->logRequest($result);
            }
        }
    }

    /**
     * Check config if SMS is enabled for website
     *
     * @param string $websiteCode
     *
     * @return bool
     */
    private function isSmsEnabled(string $websiteCode): bool
    {
        $result = $this->scopeConfig->getValue(
            self::XML_PATH_SMS_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE,
            $websiteCode
        );

        return (bool) $result;
    }

    /**
     * Check config if SMS Request log is enabled
     *
     * @return bool
     */
    private function isLoggerEnabled(): bool
    {
        return (bool) $this->scopeConfig->getValue(self::XML_PATH_SMS_ENABLED);
    }

    /**
     * Log SMS request
     *
     * @param array $request
     *
     * @return void
     */
    private function logRequest(array $request): void
    {
        $isLoggerEnabled = $this->isLoggerEnabled();
        if ($isLoggerEnabled) {
            $this->logger->debug(print_r($request, true));
        }
    }
}
