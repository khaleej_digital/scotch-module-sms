<?php
declare(strict_types=1);

namespace Beside\Sms\Model;

use Beside\Sms\Api\MessageProviderInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class MessageProvider
 *
 * @package Beside\Sms\Model
 */
class MessageProvider implements MessageProviderInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * MessageProvider constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * Get text message by message type
     *
     * @param string $type
     * @param array $customerData
     * @param int|null $storeId
     *
     * @return string|null
     */
    public function getMessageByType(string $type, array $customerData, $storeId = null): ?string
    {
        $message = '';
        switch ($type) {
            case self::SMS_TYPE_PLACE_ORDER:
                $message = $this->getConfigValue(self::XML_PATH_PLACE_ORDER_TEXT, $storeId);
                $message = str_replace('{ORDER_ID}', $customerData['order_id'], $message);
                break;
            case self::SMS_TYPE_RESET_PASSWORD:
                $message = $this->getConfigValue(self::XML_PATH_RESET_PASSWORD_TEXT, $storeId);
                $message = str_replace('{EMAIL}', $customerData['email'], $message);
                break;
            case self::SMS_TYPE_RMA_CREATE:
                $message = $this->getConfigValue(self::XML_PATH_RMA_CREATE_TEXT, $storeId);
                $message = str_replace('{ORDER_ID}', $customerData['order_id'], $message);
                break;
        }

        return $message;
    }

    /**
     * Get values from config
     *
     * @param string $path
     * @param int|null $storeId
     *
     * @return string|null
     */
    private function getConfigValue(string $path, ?int $storeId): ?string
    {
        if ($storeId === null) {
            $storeId = Store::DEFAULT_STORE_ID;
        }
        $result = $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $result;
    }
}
