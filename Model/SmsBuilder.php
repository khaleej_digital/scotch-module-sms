<?php
declare(strict_types=1);

namespace Beside\Sms\Model;

use Beside\Sms\Api\SmsInterface;
use Magento\Sales\Api\Data\OrderInterface;

/**
 * Class SmsDataBuilder
 *
 * @package Beside\Sms\Model
 */
class SmsBuilder
{
    /**
     * Get data for SMS from order
     *
     * @param OrderInterface $order
     *
     * @return array
     */
    public function getSmsDataFromOrder(OrderInterface $order): array
    {
        $shippingAddress = $order->getShippingAddress();
        $mobilePrefix = $shippingAddress->getCustomerMobileNumberPrefix() ?? '';
        $mobileNumber = $shippingAddress->getTelephone();
        $mobileNumber = $mobilePrefix . $mobileNumber;
        $storeId = (int) $order->getStoreId();
        $websiteCode = '';
        $website = $order->getStore()->getWebsite();
        if ($website) {
            $websiteCode = $website->getCode();
        }
        $customerData = [
            'email' => $order->getCustomerEmail(),
            'order_id' => (string) $order->getIncrementId()
        ];

        return [
            'customerData' => $customerData,
            SmsInterface::WEBSITE_CODE => $websiteCode,
            SmsInterface::NUMBER => $mobileNumber,
            'storeId' => $storeId
        ];
    }

    /**
     * Check if required data is collected
     *
     * @param array $smsData
     *
     * @return bool
     */
    public function isSmsDataValid(array $smsData): bool
    {
        return (!empty($smsData[SmsInterface::MESSAGE])
            && !empty($smsData[SmsInterface::WEBSITE_CODE]) && !empty($smsData[SmsInterface::NUMBER]));
    }
}
