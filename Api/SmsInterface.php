<?php
declare(strict_types=1);

namespace Beside\Sms\Api;

/**
 * Interface SmsInterface
 *
 * @package Beside\Sms\Api
 */
interface SmsInterface
{
    /** @var string Data field message */
    public const MESSAGE = 'message';

    /** @var string Data field number */
    public const NUMBER = 'mobileNumber';

    /** @var string Data field Website code */
    public const WEBSITE_CODE = 'websiteCode';

    /**
     * Get message text
     *
     * @return string
     */
    public function getMessage(): string;

    /**
     * Set message text
     *
     * @param string $message
     *
     * @return SmsInterface
     */
    public function setMessage(string $message): SmsInterface;

    /**
     * Get phone number
     *
     * @return string
     */
    public function getNumber(): string;

    /**
     * Set phone number
     *
     * @param string $number
     *
     * @return SmsInterface
     */
    public function setNumber(string $number): SmsInterface;

    /**
     * Get Website code
     *
     * @return string
     */
    public function getWebsiteCode(): string;

    /**
     * Set Website code
     *
     * @param string $websiteCode
     *
     * @return SmsInterface
     */
    public function setWebsiteCode(string $websiteCode): SmsInterface;
}
