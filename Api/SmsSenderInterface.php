<?php
declare(strict_types=1);

namespace Beside\Sms\Api;

/**
 * Interface SmsSenderInterface
 *
 * @package Beside\Sms\Api
 */
interface SmsSenderInterface
{
    /** @var string XML path to sms notification enable config */
    public const XML_PATH_SMS_ENABLED = 'beside_sms/general/enabled';

    /** @var string XML path to sms request logging enable config */
    public const XML_PATH_LOGGER_ENABLED = 'beside_sms/general/log_enable';

    /**
     * Send SMS to recipient
     *
     * @param SmsInterface $sms
     */
    public function sendSms(SmsInterface $sms): void;
}
