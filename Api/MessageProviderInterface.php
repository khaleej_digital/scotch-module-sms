<?php
declare(strict_types=1);

namespace Beside\Sms\Api;

/**
 * Interface MessageProviderInterface
 *
 * @package Beside\Sms\Api
 */
interface MessageProviderInterface
{
    /** @var string XML path to password reset message text */
    public const XML_PATH_RESET_PASSWORD_TEXT = 'beside_sms/general/reset_password_text';

    /** @var string XML path to order placement message text */
    public const XML_PATH_PLACE_ORDER_TEXT = 'beside_sms/general/place_order_text';

    /** @var string XML path to return create confirmation text */
    public const XML_PATH_RMA_CREATE_TEXT = 'beside_sms/general/rma_create_text';

    /** @var string Reset password message type */
    public const SMS_TYPE_RESET_PASSWORD = 'reset_password';

    /** @var string Place order message type */
    public const SMS_TYPE_PLACE_ORDER = 'place_order';

    /** @var string RMA create message type */
    public const SMS_TYPE_RMA_CREATE = 'rma_create';

    /**
     * Get text message by message type
     *
     * @param string $type
     * @param array $customerData
     * @param int|null $storeId
     *
     * @return string|null
     */
    public function getMessageByType(string $type, array $customerData, $storeId = null): ?string;
}
